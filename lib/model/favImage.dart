class FavImage {
  String id;
  String smallLink;
  String pathLink;
  String resolution;
  int fileSize;
  int views;
  int favorites;

  FavImage({this.id, this.smallLink, this.pathLink, this.resolution, this.fileSize, this.views, this.favorites});

  fromMap(Map<String, dynamic> json) {
    final favImage = FavImage (
        id: json['id'],
        smallLink: json['smallLink'],
        pathLink: json['pathLink'],
        resolution: json['resolution'],
        fileSize: json['fileSize'],
        views: json['views'],
        favorites: json['favorites']
    );
    return favImage;
  }

  toMap() {
    return{
      'id': id,
      'smallLink': smallLink,
      'pathLink': pathLink,
      'resolution': resolution,
      'fileSize': fileSize,
      'views': views,
      'favorites': favorites
    };
  }

  @override
  String toString() {
    return 'FavImage{id: $id, prev: $smallLink, large: $pathLink, tags: $resolution, imageSize: $fileSize, downloads: $views, likes: $favorites}';
  }

}