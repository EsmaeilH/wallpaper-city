import 'dart:math';
import 'package:dio/dio.dart';

Future<ResponsePhoto> fetchPost(
    String category, int page, String resolution) async {
  var dio = Dio();
  var index = Random().nextInt(2);
  var response;
  var apiList = ['captain', 'pixie'];
  ('captain' == apiList[index])
      ? response = await dio.get(
          'https://wallhaven.cc/api/v1/search?apikey=Yhp8mF3F86fI3uKJvuZs43I1WUSC3RzZ&q=$category&categories=101&purity=110&sorting=random&order=desc&seed=s4idA&page=$page&resolutions=$resolution')
      : response = await dio.get(
          'https://wallhaven.cc/api/v1/search?apikey=HAdc4Lg6Ov2Hwnpi2OCHESojXzKZucUG&q=$category&categories=101&purity=110&sorting=random&order=desc&seed=s4idA&page=$page&resolutions=$resolution');

  if (response.statusCode == 200) {
    // If the server returns an OK response, then parse the JSON.
    if (response.data['meta']['total'] == 0) {
      // make an error in case we don't have any result to show
      return ResponsePhoto.fromJson(response.data['meta']['total']);
    } else {
      // return correct value
      return ResponsePhoto.fromJson(response.data);
    }
  } else {
    // If the response was umexpected, throw an error.
    throw Exception('Failed to load images');
  }
}

Future<ResponsePhoto> fetchPostWithId(String id) async {
  var dio = Dio();
  final response = await dio.get('https://wallhaven.cc/api/v1/w/$id');

  if (response.statusCode == 200) {
    // If the server returns an OK response, then parse the JSON.
    return ResponsePhoto.fromJson(response.data);
  } else {
    // If the response was umexpected, throw an error.
    throw Exception('Failed to load post');
  }
}

class ResponsePhoto {
  final List<ImageResponse> images;
  final Meta meta;

  ResponsePhoto({this.images, this.meta});

  factory ResponsePhoto.fromJson(Map<String, dynamic> parsedJson) {
    var list = parsedJson['data'] as List;
    List<ImageResponse> imagesList =
        list.map((i) => ImageResponse.fromJson(i)).toList();
    return ResponsePhoto(
        images: imagesList, meta: Meta.fromJson(parsedJson["meta"]));
  }
}

class ImageResponse {
  final String id;
  final String path;
  final String resolution;
  final int fileSize;
  final int views;
  final int favorites;
  final ImageResponseThumbs thumbs;

  ImageResponse(
      {this.id,
      this.path,
      this.resolution,
      this.fileSize,
      this.views,
      this.favorites,
      this.thumbs});

  factory ImageResponse.fromJson(Map<String, dynamic> parsedJson) {
    return ImageResponse(
      id: parsedJson['id'],
      thumbs: ImageResponseThumbs.fromJson(parsedJson["thumbs"]),
      path: parsedJson['path'],
      resolution: parsedJson['resolution'],
      fileSize: parsedJson['file_size'],
      views: parsedJson['views'],
      favorites: parsedJson['favorites'],
    );
  }
}

class ImageResponseThumbs {
  final String large;
  final String original;
  final String small;

  ImageResponseThumbs({this.large, this.original, this.small});

  factory ImageResponseThumbs.fromJson(Map<String, dynamic> parsedJson) {
    return ImageResponseThumbs(
        large: parsedJson['large'] as String,
        original: parsedJson['original'] as String,
        small: parsedJson['small'] as String);
  }
}

class Meta {
  final int total;

  Meta({this.total});

  factory Meta.fromJson(Map<String, dynamic> parsedJson) {
    return Meta(
      total: parsedJson['total'],
    );
  }
}
