import 'package:flutter/material.dart';
import 'package:admob_flutter/admob_flutter.dart';
import 'package:debug_mode/debug_mode.dart';
import 'package:package_info/package_info.dart';
import 'package:ext_storage/ext_storage.dart';
import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:path_provider/path_provider.dart';
import 'package:rate_my_app/rate_my_app.dart';
import 'package:wall_paper/services/admobService.dart';
import 'package:wall_paper/services/globals.dart';

class Setting extends StatefulWidget {
  @override
  _MySettingState createState() => _MySettingState();

}

class _MySettingState extends State<Setting> {

  TextStyle _txtStyle = TextStyle(fontSize: 20);
  AdmobReward rewardAd;
  String path = '';
  String appURI = 'https://play.google.com/store/apps/details?id=com.Wallpcity.wall_paper';
  bool _isRewardAdLoaded = false;
  RateMyApp _rateMyApp;

  PackageInfo _packageInfo = PackageInfo(
    // appName: 'Unknown',
    // packageName: 'Unknown',
    version: 'Unknown',
    // buildNumber: 'Unknown',
  );

  void initState () {
    super.initState();
     _initPackageInfo();
     _rateMyApp = RateMyApp(googlePlayIdentifier: "com.Wallpcity.wall_paper");
     _rateMyApp.init();

    _getDownloadDirectory().then((String value) {
      path = value;
    });

    rewardAd = AdmobReward(
        adUnitId: DebugMode.isInDebugMode ? AdmobService.testRewardID : AdmobService.rewardID,
        listener: (AdmobAdEvent event, Map<String, dynamic> args) {
          if (event == AdmobAdEvent.closed) rewardAd.load();
          handleEvent(event, args, 'Reward');
        });

    rewardAd.load();
  }

  @override
  Widget build(BuildContext context){

    return Scaffold(
      appBar: AppBar(
        title: Text('Setting'),
        centerTitle: true,
        backgroundColor: Colors.transparent,
      ),
      body: ListView(
        children: [
          ListTile(
            leading: Icon(Icons.play_circle_outline),
            title: Text('Remove Ads',style: _txtStyle,),
            subtitle: Padding(
              padding: const EdgeInsets.only(left: 5),
              child: Text('Enjoy Ad-Free version'),
            ),
            onTap: () {
              rewardDialog();
            },
          ),
          Divider(color: Colors.deepOrange,),
          ListTile(
            leading: Icon(Icons.storage),
            title: Text('Storage Location', style: _txtStyle,),
            subtitle: Padding(
              padding: const EdgeInsets.only(left: 5),
              child: Text(path),
            ),
            onTap: (){},
          ),
          Divider(color: Colors.deepOrange,),
          ListTile(
            leading: Icon(Icons.share),
            title: Text('Share with friends', style: _txtStyle,),
            subtitle: Padding(
              padding: const EdgeInsets.only(left: 5),
              child: Text('Recommend to your friends'),
            ),
            onTap: () {
              Share.text('App link...', appURI, 'text/plain');
            },
          ),
          Divider(color: Colors.deepOrange,),
          ListTile(
            leading: Icon(Icons.cached),
            title: Text('Clear cache', style: _txtStyle,),
            subtitle: Padding(
              padding: const EdgeInsets.only(left: 5),
              child: Text('Clear app cache folder'),
            ),
            onTap: () {
              _deleteCacheDir();
            },
          ),
          Divider(color: Colors.deepOrange,),
          ListTile(
            leading: Icon(Icons.rate_review),
            title: Text('Rate us', style: _txtStyle,),
            subtitle: Padding(
              padding: const EdgeInsets.only(left: 5),
              child: Text('rate us if you like this app'),
            ),
            onTap: () {
              _rateMyApp.showRateDialog(
                context,
                title: 'Rate this app', // The dialog title.
                message: 'If you like this app, please take a little bit of your time to review and rate us.', // The dialog message.
                rateButton: 'RATE', // The dialog "rate" button text.
                noButton: 'NO THANKS', // The dialog "no" button text.
                laterButton: 'MAYBE LATER', // The dialog "later" button text.
              );
            },
          ),
          Divider(color: Colors.deepOrange,),
          ListTile(
            leading: Icon(Icons.android),
            title: Text('Version', style: _txtStyle,),
            subtitle: Padding(
              padding: const EdgeInsets.only(left: 5),
              child: Text(_packageInfo.version),
            ),
            onTap: (){},
          ),
        ],
      )
    );
  }

  Future rewardDialog () {
    return showDialog(
        context: context,
        builder: (BuildContext context) => AlertDialog(
          title: Center(
              child: (!isRewarded) ?
              Text('Normal mode', style: TextStyle(color: Colors.deepOrange, fontSize: 30),)
                  :
              Text('Pro mode', style: TextStyle(color: Colors.lightBlue, fontSize: 30),)
          ),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0)
          ),
          content: AspectRatio(
            aspectRatio: 4/7,
            child: Flex(
              direction: Axis.vertical,
              children: [
                FittedBox(child: Text('Watch ads to enjoy ads free version''\n till next time open the app again.', style: TextStyle(fontSize: 20),)),
                Expanded(flex: 7,child: Image.asset('assets/Icon/playstore.png')),
                Expanded(
                  flex: 1,
                  child: RaisedButton(
                    padding: EdgeInsets.fromLTRB(80, 5, 80, 5),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20)
                    ),
                    onPressed: () {
                      if(_isRewardAdLoaded) {
                        _adsLoadingDialog();
                        Future.delayed(Duration(seconds: 2), () {
                          Navigator.pop(context);
                        });
                      }else {
                        rewardAd.show();
                      }
                    },
                    child: Text('Show Ads'),
                  ),
                )
                // Container(
                //   height: 400,
                //   child: Column(
                //     children: [
                //       // SizedBox(height: 20,),
                //
                //       SizedBox(height: 25,),
                //     ],
                //   ),
                // ),
              ],
            ),
          ),
        )
    );
  }

  void handleEvent(AdmobAdEvent event, Map<String, dynamic> args, String adType) {
    switch (event) {
      case AdmobAdEvent.loaded:
        print('New Admob $adType Ad loaded!');
        setState(() {
          _isRewardAdLoaded = true;
        });
        break;
      case AdmobAdEvent.opened:
        print('Admob $adType Ad opened!');
        break;
      case AdmobAdEvent.closed:
        print('Admob $adType Ad closed!');
        setState(() {
          _isRewardAdLoaded = false;
        });
        break;
      case AdmobAdEvent.failedToLoad:
        print('Admob $adType failed to load. :(');
        setState(() {
          _isRewardAdLoaded = false;
        });
        break;
      case AdmobAdEvent.rewarded:
        print('rewarded Ad served completly');
        setState(() {
          isRewarded = true;
        });
        break;
      default:
    }
  }

  Future<String> _getDownloadDirectory() async {

      return await ExtStorage.getExternalStoragePublicDirectory(
          ExtStorage.DIRECTORY_PICTURES);

  }

  Future<void> _initPackageInfo() async {
    final PackageInfo info = await PackageInfo.fromPlatform();
    setState(() {
      _packageInfo = info;
    });
  }

  Future _deleteCacheDir() async {
    final cacheDir = await getTemporaryDirectory();
    _clearCacheDialog();

    if (cacheDir.existsSync()) {
      cacheDir.deleteSync(recursive: true);
      Future.delayed(Duration(seconds: 1),() {
        Navigator.pop(context);
      });
    }
  }

  Future _clearCacheDialog() {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) => AlertDialog(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20.0)
        ),
        content: FittedBox(
          fit: BoxFit.contain,
          child: Container(
            height: 40,
              child: Center(
                  child: RichText(
                    text: TextSpan(
                      children: [
                        TextSpan(
                          text: "Cache Cleared ",
                          style: TextStyle(fontSize: 20,)
                        ),
                        WidgetSpan(
                            child: Icon(Icons.check)
                        )
                      ]
                    ),
                  )
              )
          ),
        ),
        backgroundColor: Colors.transparent,
      )
    );
  }

  Future _adsLoadingDialog() {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) => AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0)
          ),
          content: FittedBox(
            child: Flex(
                direction: Axis.horizontal,
              children: [
                Text('Loading, try again...  ', style: TextStyle(fontSize: 20),),
                CircularProgressIndicator()
              ],
            ),
          ),

          // Container(
          //     height: 40,
          //     child:
          //     Center(
          //         child: RichText(
          //           text: TextSpan(
          //               children: [
          //                 TextSpan(
          //                     text: "Loading, try again ...     ",
          //                     style: TextStyle(fontSize: 20,)
          //                 ),
          //                 WidgetSpan(
          //                   style: TextStyle(height: 5),
          //                     child: CircularProgressIndicator()
          //                 )
          //               ]
          //           ),
          //         )
          //     )
          // ),
          backgroundColor: Colors.transparent,
        )
    );
  }

  void dispose() {
    rewardAd.dispose();
    super.dispose();
  }
}
