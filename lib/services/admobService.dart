
import 'package:admob_flutter/admob_flutter.dart';
import 'package:debug_mode/debug_mode.dart';

class AdmobService {
  static const String bannerID = 'ca-app-pub-7507286730673285/7628737723';
  static const String interstitialID = 'ca-app-pub-7507286730673285/7205872374';
  static const String rewardID = 'ca-app-pub-7507286730673285/5780479178';
  static const String appID = 'ca-app-pub-7507286730673285~6877094539';
  static const String test_appID = 'ca-app-pub-3940256099942544~3347511713';
  static const String testBannerID = 'ca-app-pub-3940256099942544/6300978111';
  static const String testInterstitialID = 'ca-app-pub-3940256099942544/1033173712';
  static const String testRewardID = 'ca-app-pub-3940256099942544/5224354917';

  Admob initAdMob() {
    return Admob.initialize(DebugMode.isInDebugMode ? test_appID : appID);
  }

  String getBannerAdUnitId() {
    return bannerID;
  }

  String getInterstitialAdUnitId() {
    return interstitialID;
  }

  String getRewardAdUnitId() {
    return rewardID;
  }

  String getBannerAdUnitIdTest() {
    return testBannerID;
  }

  String getInterstitialAdUnitIdTest() {
    return testInterstitialID;
  }

  String getRewardAdUnitIdTest() {
    return testRewardID;
  }

}