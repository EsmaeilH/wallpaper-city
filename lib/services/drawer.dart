import 'package:flutter/material.dart';
import 'package:wall_paper/services/globals.dart';
import 'package:admob_flutter/admob_flutter.dart';
import 'package:wall_paper/services/admobService.dart';
import 'package:debug_mode/debug_mode.dart';

import '../secondPage.dart';

class WallpDrawer extends StatefulWidget {
  @override
  _WallpDrawerState createState() => _WallpDrawerState();
}

class _WallpDrawerState extends State<WallpDrawer> {
  var _searchFieldController = TextEditingController();
  final admobService = AdmobService();
  AdmobInterstitial interstitialAd;

  @override
  void initState() {
    super.initState();
    interstitialAd = AdmobInterstitial(
      adUnitId: DebugMode.isInDebugMode ? admobService.getInterstitialAdUnitIdTest() : admobService.getInterstitialAdUnitId(),
      listener: (AdmobAdEvent event, Map<String, dynamic> args) {
        if (event == AdmobAdEvent.closed) interstitialAd.load();
      },
    );
    interstitialAd.load();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width * 0.7,
      height: MediaQuery.of(context).size.height,
      child: Container(
        padding: EdgeInsets.fromLTRB(0, 80, 0, 0),
        child: ListView(
          children: <Widget>[
            ListTile(
              title: TextField(
                controller: _searchFieldController,
                textInputAction: TextInputAction.search,
                onSubmitted: (String str){
                  if (!isRewarded) {
                    interstitialAd.show();
                  }
                  Navigator.pop(context);
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) =>
                            SecondPage(_searchFieldController.text),
                      ));
                },
                decoration: InputDecoration(
                  hintText: 'Search',
                  suffixIcon: IconButton(
                      icon: Icon(Icons.search),
                      onPressed: () {
                        if (_searchFieldController.text != '') {
                          if (!isRewarded) {
                            interstitialAd.show();
                          }
                          Navigator.pop(context);
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) =>
                                    SecondPage(_searchFieldController.text),
                              ));
                        }
                      })),
            )),
            ListTile(
              title: Column(
                children: <Widget>[
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                      height: 30,
                      width: double.maxFinite,
                      padding:
                      EdgeInsets.all(7),
                      decoration: BoxDecoration(
                        color: (resolution == '1920x1080')
                            ? Colors.deepOrange
                            : Colors.transparent,
                        borderRadius: BorderRadius.circular(5),
                      ),
                      child: InkWell(
                        child: (resolution == '1920x1080')
                            ? Text(
                                '1920x1080 (full hd)',
                                style: TextStyle(color: Colors.white, fontSize: 16),
                              )
                            : Text(
                                '1920x1080 (full hd)',
                                style: TextStyle(fontSize: 16),
                              ),
                        onTap: () {
                          setState(() {
                            resolution = '1920x1080';
                          });
                        },
                      )),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                      height: 30,
                      width: double.maxFinite,
                      padding:
                      EdgeInsets.all(7),
                      decoration: BoxDecoration(
                        color: (resolution == '3840x2160')
                            ? Colors.deepOrange
                            : Colors.transparent,
                        borderRadius: BorderRadius.circular(5),
                      ),
                      child: InkWell(
                        child: (resolution == '3840x2160')
                            ? Text(
                                '3840x2160 (4k)',
                                style: TextStyle(color: Colors.white, fontSize: 16),
                              )
                            : Text(
                                '3840x2160 (4k)',
                                style: TextStyle(fontSize: 16),
                              ),
                        onTap: () {
                          setState(() {
                            resolution = '3840x2160';
                          });
                        },
                      )),
                ],
              ),
            ),
            Divider(
              color: Colors.deepOrange,
            ),
            ListTile(
              title: Text('Architecture', style: TextStyle(fontSize: MediaQuery.of(context).size.width/17),),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => SecondPage('Architecture'),
                    ));
              },
            ),
            ListTile(
              title: Text('Animal', style: TextStyle(fontSize: MediaQuery.of(context).size.width/17),),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => SecondPage('Animal'),
                    ));
              },
            ),
            ListTile(
              title: Text('Dark', style: TextStyle(fontSize: MediaQuery.of(context).size.width/17),),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => SecondPage('Dark'),
                    ));
              },
            ),
            ListTile(
              title: Text('Sport', style: TextStyle(fontSize: MediaQuery.of(context).size.width/17),),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => SecondPage('Sport'),
                    ));
              },
            ),
            ListTile(
              title: Text('Space', style: TextStyle(fontSize: MediaQuery.of(context).size.width/17),),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => SecondPage('Space'),
                    ));
              },
            ),
            ListTile(
              title: Text('Fashion', style: TextStyle(fontSize: MediaQuery.of(context).size.width/17),),
              onTap: () {
                // Update the state of the app
                // ...
                // Then close the drawer
                Navigator.pop(context);
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => SecondPage('Fashion'),
                    ));
              },
            ),
            ListTile(
              title: Text('Nature', style: TextStyle(fontSize: MediaQuery.of(context).size.width/17),),
              onTap: () {
                // Update the state of the app
                // ...
                // Then close the drawer
                Navigator.pop(context);
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => SecondPage('Nature'),
                    ));
              },
            ),
            ListTile(
              title: Text('Fantasy', style: TextStyle(fontSize: MediaQuery.of(context).size.width/17),),
              onTap: () {
                // Update the state of the app
                // ...
                // Then close the drawer
                Navigator.pop(context);
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => SecondPage('Fantasy'),
                    ));
              },
            ),
            ListTile(
              title: Text('Mountain', style: TextStyle(fontSize: MediaQuery.of(context).size.width/17),),
              onTap: () {
                // Update the state of the app
                // ...
                // Then close the drawer
                Navigator.pop(context);
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => SecondPage('Mountain'),
                    ));
              },
            ),
            ListTile(
              title: Text('Food', style: TextStyle(fontSize: MediaQuery.of(context).size.width/17),),
              onTap: () {
                // Update the state of the app
                // ...
                // Then close the drawer
                Navigator.pop(context);
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => SecondPage('Food'),
                    ));
              },
            ),
            ListTile(
              title: Text('Flower', style: TextStyle(fontSize: MediaQuery.of(context).size.width/17),),
              onTap: () {
                // Update the state of the app
                // ...
                // Then close the drawer
                Navigator.pop(context);
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => SecondPage('Flower'),
                    ));
              },
            ),
            ListTile(
              title: Text('Love', style: TextStyle(fontSize: MediaQuery.of(context).size.width/17),),
              onTap: () {
                // Update the state of the app
                // ...
                // Then close the drawer
                Navigator.pop(context);
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => SecondPage('Love'),
                    ));
              },
            ),
            ListTile(
              title: Text('Travel', style: TextStyle(fontSize: MediaQuery.of(context).size.width/17),),
              onTap: () {
                // Update the state of the app
                // ...
                // Then close the drawer
                Navigator.pop(context);
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => SecondPage('Travel'),
                    ));
              },
            ),
            ListTile(
              title: Text('Music', style: TextStyle(fontSize: MediaQuery.of(context).size.width/17),),
              onTap: () {
                // Update the state of the app
                // ...
                // Then close the drawer
                Navigator.pop(context);
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => SecondPage('Music'),
                    ));
              },
            ),
            ListTile(
              title: Text('Construction', style: TextStyle(fontSize: MediaQuery.of(context).size.width/17),),
              onTap: () {
                // Update the state of the app
                // ...
                // Then close the drawer
                Navigator.pop(context);
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => SecondPage('Construction'),
                    ));
              },
            ),
            ListTile(
              title: Text('Religion', style: TextStyle(fontSize: MediaQuery.of(context).size.width/17),),
              onTap: () {
                // Update the state of the app
                // ...
                // Then close the drawer
                Navigator.pop(context);
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => SecondPage('Religion'),
                    ));
              },
            ),
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    interstitialAd.dispose();
    _searchFieldController.dispose();
  }
}
