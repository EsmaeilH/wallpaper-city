import 'package:flutter/material.dart';
import 'package:flutter/services.dart' ;
import 'package:wall_paper/wallpapermainpage.dart';
import 'package:wall_paper/services/admobService.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  final admobService = AdmobService();
  admobService.initAdMob();
  runApp(MaterialApp(
    theme: ThemeData(
      brightness: Brightness.dark,
      primarySwatch: Colors.orange,
      indicatorColor: Colors.deepOrange,
      hintColor: Colors.deepOrange,
    ),
    home: WallPaperMainPage(),
  ));
}
