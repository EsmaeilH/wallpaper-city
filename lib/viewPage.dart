import 'dart:async';
import 'dart:io';
import 'dart:ui';
import 'dart:typed_data';

import 'package:admob_flutter/admob_flutter.dart';
import 'package:wall_paper/services/admobService.dart';
import 'package:debug_mode/debug_mode.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:wallpaper_manager/wallpaper_manager.dart';
import 'package:wall_paper/model/favImage.dart';
import 'package:path/path.dart' as path;
import 'package:wall_paper/wallpapermainpage.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:ext_storage/ext_storage.dart';
import 'package:wall_paper/model/fetchImage.dart';
import 'package:wall_paper/services/globals.dart';

class ViewPage extends StatelessWidget {
  final ImageResponse indexedImage;

  ViewPage(this.indexedImage);

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: Page(indexedImage));
  }
}

class Page extends StatefulWidget {
  final ImageResponse indexedImage;

  Page(this.indexedImage);

  @override
  PageState createState() => PageState(indexedImage);
}

class PageState extends State<Page> {
  var indexedImage;

  PageState(this.indexedImage);

  final Dio _dio = Dio();
  final admobService = AdmobService();

  String _progressString = "-";

  StreamController<double> progressStream;
  var db = WallPaperMainPage.instance;
  bool alreadySaved = false;
  bool _showOrHide = true;
  int cnt = 0;
  Timer _timer;
  bool urlValidation = false;
  bool isDownloaded = false;
  var savePath;
  var croppedPath;
  var didDownloadButtonPressed = false;

  // CustomCacheManager _instance;

  @override
  void initState() {
    super.initState();

    progressStream = new StreamController.broadcast();

    if(previousPage == 'favoritePage'){
      _checkUrlValidation(indexedImage.id);
    }
  }

  void _checkUrlValidation(String id) async {
    var response2;
    response2 = await _dio.head(indexedImage.path);
    if (response2.statusCode == 400) {
      var response;
      response = await fetchPostWithId(id);
      indexedImage = ImageResponse(
          id: response.images[0].id,
          thumbs: response.images[0].thumbs,
          path: response.images[0].path,
          resolution: response.images[0].resolution,
          fileSize: response.images[0].fileSize,
          views: response.images[0].views,
          favorites: response.images[0].favorites
      );
      if (response.images[0].id != null) {
        setState(() {
          urlValidation = true;
        });
      }
      var temp = FavImage(
        id: indexedImage.id,
        smallLink: indexedImage.thumbs.small,
        pathLink: indexedImage.path,
        resolution: indexedImage.resolution,
        fileSize: indexedImage.fileSize,
        views: indexedImage.views,
        favorites: indexedImage.favorites,
      );
      db.update(temp);
    }
  }

  _croppedImage(String imagePath) async{
    File cropped = await ImageCropper.cropImage(
      sourcePath: imagePath,
      compressQuality: 100,
      compressFormat: ImageCompressFormat.jpg,
      androidUiSettings: AndroidUiSettings(
        toolbarColor: Colors.deepOrange,
        toolbarTitle: 'Crop and Set'
      )
    );

    croppedPath = cropped.path;
  }

  Future<String> _getDownloadDirectory() async {
    if (Platform.isAndroid) {
      return await ExtStorage.getExternalStoragePublicDirectory(
          ExtStorage.DIRECTORY_PICTURES);
    }
  }

  Future<bool> _requestPermissions() async {
    var permission = await PermissionHandler()
        .checkPermissionStatus(PermissionGroup.storage);

    if (permission != PermissionStatus.granted) {
      await PermissionHandler().requestPermissions([PermissionGroup.storage]);
      permission = await PermissionHandler()
          .checkPermissionStatus(PermissionGroup.storage);
    }

    return permission == PermissionStatus.granted;
  }

  void _onReceiveProgress(int received, int total) {
    setState(() {
      progressStream.add((received / total * 100));
      _progressString = (received / total * 100).toStringAsFixed(0) + "%";
    });
  }

  Future<void> _startDownload(String savePath, String url) async {
    Map<String, dynamic> result = {
      'isSuccess': false,
      'filePath': null,
      'error': null,
    };

    try {
      final response = await _dio.download(url, savePath,
          onReceiveProgress: _onReceiveProgress);
      result['isSuccess'] = response.statusCode == 200;
      if (result['isSuccess']) {
        (didDownloadButtonPressed) ?
        Future.delayed(Duration(seconds: 1), () {
          Navigator.of(context).pop();
          _showToast(context,'Download Completed!');
          _progressString = '-';
          isDownloaded = true;
        }):
        _progressString = '-';
        setState(() {
          isDownloaded = true;
        });
      }
      result['filePath'] = savePath;
    } catch (ex) {
      result['error'] = ex.toString();
    } finally {
      // await _showNotification(result);
    }
  }

  Future<void> download(String url, String fileName) async {
    final dir = await _getDownloadDirectory();
    final isPermissionStatusGranted = await _requestPermissions();

    if (isPermissionStatusGranted) {
      savePath = path.join(dir, '$fileName.jpg');
      await _startDownload(savePath, url);
    } else {
      Navigator.pop(context);
      _accessDeniedDialog();
      Future.delayed(Duration(seconds: 2), () {
        Navigator.pop(context);
      });
    }
  }

  _showToast(BuildContext context,text) {
    final scaffold = Scaffold.of(context);
    scaffold.showSnackBar(
      SnackBar(
        duration: Duration(seconds: 1),
        content:  Text(text),
      ),
    );
  }

  _showSetWallpaperToast(text) {
    _showToast(context, text);
  }

  _findRecords(String id) async {
    int result = await db.findOne(indexedImage.id);
    if (cnt == 0) {
      _timer = Timer(Duration(milliseconds: 10), () {
        setState(() {
          if (result == 0) {
            alreadySaved = false;
          } else {
            alreadySaved = true;
          }
        });
      });
      cnt++;
    } else {
      cnt = 0;
    }
  }

  Future<void> didPressActionButton() async {
    var request =
        await HttpClient().getUrl(Uri.parse(indexedImage.path));
    var response = await request.close();
    Uint8List bytes = await consolidateHttpClientResponseBytes(response);
    await Share.file('my shared image', 'amlog.jpg', bytes, 'image/jpg');
  }

  Future setWallpaperDialog() {
    return showDialog(
        context: context,
      builder: (BuildContext context) => StreamBuilder(
        stream: progressStream.stream,
          builder: (BuildContext context, AsyncSnapshot<double> snapshot) {
            return AlertDialog(
              title: (isDownloaded) ?Center(
                child: Text('Set Wallpaper as...'),
              )
              : Center(
                child: Text('downloading $_progressString'),
              ),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)
              ),
              content: Container(
                height: 150,
                child:(isDownloaded) ? Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Divider(color: Colors.blueAccent,),
                    Container(
                      height: 30,
                      width: double.maxFinite,
                      child: Row(
                        children: [
                          Icon(Icons.add_to_home_screen),
                          Container(
                            width: 200,
                            child: InkWell(child: Text('HomeScreen', style: TextStyle(fontSize: 20),), onTap: ()async{
                              int location = WallpaperManager.HOME_SCREEN;
                              await _croppedImage(savePath);
                              final String result = await WallpaperManager.setWallpaperFromFile(croppedPath, location);
                              Navigator.of(context).pop();
                              _showSetWallpaperToast(result);
                            }),
                          ),
                        ],
                      ),
                    ),
                    Divider(color: Colors.deepOrange,),
                    Container(
                      height: 30,
                      width: double.maxFinite,
                      child: Row(
                        children: [
                          Icon(Icons.screen_lock_portrait),
                          Container(
                            width: 200,
                            child: InkWell(child: Text('LockScreen', style: TextStyle(fontSize: 20),), onTap: ()async{
                              int location = WallpaperManager.LOCK_SCREEN;
                              await _croppedImage(savePath);
                              final String result = await WallpaperManager.setWallpaperFromFile(savePath, location);
                              Navigator.of(context).pop();
                              _showSetWallpaperToast(result);
                            }),
                          ),
                        ],
                      ),
                    ),
                    Divider(color: Colors.deepOrange,),
                    Container(
                      height: 30,
                      width: double.maxFinite,
                      child: Row(
                        children: [
                          Icon(Icons.fullscreen),
                          Container(
                            width: 200,
                            child: InkWell(child: Text('BothScreen', style: TextStyle(fontSize: 20),), onTap: ()async{
                              int location = WallpaperManager.BOTH_SCREENS;
                              await _croppedImage(savePath);
                              final String result = await WallpaperManager.setWallpaperFromFile(savePath, location);
                              Navigator.of(context).pop();
                              _showSetWallpaperToast(result);
                            }),
                          ),
                        ],
                      ),
                    ),
                  ],
                ) :
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Divider(color: Colors.blueAccent,),
                    Container(
                      height: 30,
                      width: double.maxFinite,
                      child: Row(
                        children: [
                          Icon(Icons.add_to_home_screen),
                          InkWell(child: Text('HomeScreen', style: TextStyle(fontSize: 20),), onTap: null),
                        ],
                      ),
                    ),
                    Divider(color: Colors.deepOrange,),
                    Container(
                      height: 30,
                      width: double.maxFinite,
                      child: Row(
                        children: [
                          Icon(Icons.screen_lock_portrait),
                          InkWell(child: Text('LockScreen', style: TextStyle(fontSize: 20),), onTap: null),
                        ],
                      ),
                    ),
                    Divider(color: Colors.deepOrange,),
                    Container(
                      height: 30,
                      width: double.maxFinite,
                      child: Row(
                        children: [
                          Icon(Icons.fullscreen),
                          InkWell(child: Text('BothScreen', style: TextStyle(fontSize: 20),), onTap: null),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              backgroundColor: Colors.transparent,
              elevation: 1,
            );
          }
      )
    );
  }

  Future downloadDialog() {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) => StreamBuilder(
            stream: progressStream.stream,
            builder: (BuildContext context, AsyncSnapshot<double> snapshot) {
              return AlertDialog(
                title: Center(
                  child: Text(_progressString),
                ),
                titleTextStyle: TextStyle(color: Colors.white),
                contentTextStyle: TextStyle(color: Colors.white),
                content: Container(
//                  height: 10,
                  child: LinearProgressIndicator(
//                    value: _progressDouble,
                    backgroundColor: Colors.cyanAccent,
                  ),
                ),
                backgroundColor: Colors.transparent,
                elevation: 1,
              );
            }));
  }

  Future detailShowDialog() {
    return showDialog(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        title: Center(child: Text('Details')),
        titleTextStyle: TextStyle(color: Colors.white),
        contentTextStyle: TextStyle(color: Colors.white),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20.0)
        ),
        content: Container(
          height: 90,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Icon(
                    Icons.tag_faces,
                    color: Colors.white,
                  ),
                  Icon(
                    Icons.file_download,
                    color: Colors.white,
                  ),
                  Icon(
                    Icons.favorite,
                    color: Colors.white,
                  )
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(indexedImage.resolution),
                  Text((indexedImage.fileSize / 1000000).toStringAsFixed(2) + ' MB'),
                  Text(indexedImage.favorites.toString())
                ],
              ),
            ],
          ),
        ),
        backgroundColor: Colors.transparent,
        elevation: 1,
      ),
    );
  }

  Future _accessDeniedDialog() {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) => AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0)
          ),
          content: Container(
              height: 100,
              child: Column(
                children: [
                  Center(
                      child: Text("Access denied by user", style: TextStyle(fontSize: 19, color: Colors.deepOrange),),
                  ),
                  SizedBox(height: 20,),
                  Icon(Icons.block , size: 50, color: Colors.red,),
                ],
              )
          ),
          backgroundColor: Colors.transparent,
        )
    );
  }

  @override
  Widget build(BuildContext context) {
    _findRecords(indexedImage.id);
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        actions: <Widget>[
          GestureDetector(
            child: IconButton(icon: new Icon(Icons.info_outline), onPressed: (){
              detailShowDialog();
            }),
          ),
        ],
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 1,
      ),
      body: Stack(
        children: <Widget>[
          GestureDetector(
              onLongPress: () {
                setState(() {
                  _showOrHide = false;
                });
              },
              onLongPressEnd: (e) {
                setState(() {
                  _showOrHide = true;
                });
              },
              child: CachedNetworkImage(
                imageUrl: indexedImage.path,
                imageBuilder: (context, imageProvider) => Container(
                  decoration: BoxDecoration(
                      image: DecorationImage(
                    image: imageProvider,
                    fit: BoxFit.cover,
                  )),
                ),
              )),
          Visibility(
            visible: _showOrHide,
            child: Align(
              alignment: const Alignment(0.5, 0.9),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Opacity(
                        opacity: .6,
                        child: InkWell(
                          onTap: () {
                            didDownloadButtonPressed = true;
                            download(indexedImage.path,
                                indexedImage.id);
                            downloadDialog();
                          },
                          child: ClipOval(
                            child: Container(
                              color: Colors.blueGrey,
                              height: 70,
                              width: 70,
                              child: Icon(
                                Icons.file_download,
                                color: Colors.white.withOpacity(1),
                                size: 50,
                              ),
                            ),
                          ),
                        ),
                      ),
                      Opacity(
                        opacity: .6,
                        child: InkWell(
                          onTap: () {
                            setState(() {
                              if (alreadySaved) {
                                db.delete(indexedImage.id);
                              } else {
                                var temp = FavImage(
                                    id: indexedImage.id,
                                    smallLink: indexedImage.thumbs.small,
                                    pathLink: indexedImage.path,
                                    resolution: indexedImage.resolution,
                                    fileSize: indexedImage.fileSize,
                                    views: indexedImage.views,
                                    favorites: indexedImage.favorites);
                                db.insert(temp);
                              }
                            });
                          },
                          child: ClipOval(
                            child: Container(
                              color: Colors.blueGrey,
                              height: 70,
                              width: 70,
                              child: Icon(
                                alreadySaved
                                    ? Icons.favorite
                                    : Icons.favorite_border,
                                color: alreadySaved
                                    ? Colors.red
                                    : Colors.white.withOpacity(1),
                                size: 50,
                              ),
                            ),
                          ),
                        ),
                      ),
                      Opacity(
                        opacity: .6,
                        child: InkWell(
                          onTap: () {
                            didPressActionButton();
                          },
                          child: ClipOval(
                            child: Container(
                              color: Colors.blueGrey,
                              height: 70,
                              width: 70,
                              child: Icon(
                                Icons.share,
                                color: Colors.white.withOpacity(1),
                                size: 50,
                              ),
                            ),
                          ),
                        ),
                      ),
                      Opacity(
                        opacity: .6,
                        child: InkWell(
                          onTap: () {
                            didDownloadButtonPressed = false;
                            if (!isDownloaded) {
                              download(indexedImage.path, indexedImage.id);
                            }
                            setWallpaperDialog();
                          },
                          child: ClipOval(
                            child: Container(
                              color: Colors.blueGrey,
                              height: 70,
                              width: 70,
                              child: Icon(
                                Icons.wallpaper,
                                color: Colors.white.withOpacity(1),
                                size: 50,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  (!isRewarded) ?
                  Padding(
                    padding: const EdgeInsets.only(top: 5),
                    child: AdmobBanner(
                      adUnitId: DebugMode.isInDebugMode ? admobService.getBannerAdUnitIdTest() : admobService.getBannerAdUnitId(),
                      adSize: AdmobBannerSize.BANNER,
                    ),
                  ) :
                  SizedBox(height: 50,)
                ],),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }
}

