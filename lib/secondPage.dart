import 'package:flutter/material.dart';
import 'package:wall_paper/viewPage.dart';
import 'package:wall_paper/favouritePage.dart';
import 'package:wall_paper/services/globals.dart';
import 'package:wall_paper/model/fetchImage.dart';
import 'package:lottie/lottie.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:admob_flutter/admob_flutter.dart';
import 'package:wall_paper/services/admobService.dart';
import 'package:debug_mode/debug_mode.dart';
import 'package:cached_network_image/cached_network_image.dart';

class SecondPage extends StatefulWidget {
  final String cateG;

  SecondPage(this.cateG);

  @override
  SecondPageState createState() => SecondPageState(cateG);
}

class SecondPageState extends State<SecondPage> {
  final String category;

  SecondPageState(this.category);

  Future<ResponsePhoto> resp;

  ScrollController _scrollController;

  int pageNumber = 1;
  int totalHits = 0;
  List imagelist = [];
  bool bannerAdFailed = false;
  final admobService = AdmobService();

  @override
  void initState() {
    super.initState();
    if (imagelist.length == 0) {
      resp = fetchPost(category, pageNumber, resolution);
    }

    _scrollController = ScrollController();
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
              _scrollController.position.maxScrollExtent &&
          imagelist.length < totalHits) {
        setState(() {
          pageNumber++;
          resp = fetchPost(category, pageNumber, resolution);
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder<ResponsePhoto>(
        future: resp,
        builder: (context, obj) {
          if (obj.hasData) {
            if (!imagelist.contains(obj.data.images[0])) {
              totalHits = obj.data.meta.total;
              imagelist.addAll(obj.data.images);
            }

            return _staggeredGridWidget();
          } else if (obj.hasError) {
            return Center(child: Lottie.asset('assets/empty.json'));
          }
          return Center(child: CircularProgressIndicator());
        },
      ),
    );
  }

  Widget _staggeredGridWidget() {
    return CustomScrollView(
      controller: _scrollController,
      slivers: [
        SliverAppBar(
          floating: true,
          actions: <Widget>[
            IconButton(
                icon: new Icon(Icons.favorite_border),
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => FavouritePage()));
                }),
          ],
          flexibleSpace: FlexibleSpaceBar(
            centerTitle: true,
            title: Text(
              '$category',
              style:
                  TextStyle(fontSize: MediaQuery.of(context).size.width / 18),
            ),
          ),
        ),
        SliverStaggeredGrid.countBuilder(
          crossAxisCount: 3,
          staggeredTileBuilder: (int index) => new StaggeredTile.count(
              (index % 18 == 0) ? 3 : 1,
              (index.isEven && index % 18 != 16) ? 2 : 1),
          itemBuilder: (BuildContext context, int index) {
            return (index % 18 == 0 && index != 0 && !bannerAdFailed && !isRewarded)
                ? AdmobBanner(
                    adUnitId: DebugMode.isInDebugMode
                        ? admobService.getBannerAdUnitIdTest()
                        : admobService.getBannerAdUnitId(),
                    adSize: AdmobBannerSize.MEDIUM_RECTANGLE,
                    listener: (AdmobAdEvent event, Map<String, dynamic> args) {
                      handleEvent(event);
                    },
                  )
                : Stack(
                    children: <Widget>[
                      Container(
                        child: CachedNetworkImage(
                          imageUrl:
                              (index.isEven || index % 18 == 17 || index == 0)
                                  ? imagelist[index].thumbs.large
                                  : imagelist[index].thumbs.small,
                          imageBuilder: (context, imageProvider) => Container(
                            decoration: BoxDecoration(
                                image: DecorationImage(
                              image: imageProvider,
                              fit: BoxFit.cover,
                            )),
                          ),
                        ),
                      ),
                      Material(
                        type: MaterialType.transparency,
                        child: InkWell(
                          onTap: () {
                            previousPage = 'secondPage';
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      ViewPage(imagelist[index])),
                            );
                          },
                        ),
                      )
                    ],
                  );
          },
          itemCount: imagelist.length,
          mainAxisSpacing: 4,
          crossAxisSpacing: 4,
        ),
        imagelist.length < totalHits
            ? SliverToBoxAdapter(
                child: Center(child: CircularProgressIndicator()),
              )
            : SliverToBoxAdapter(
                child: Center(
                child: Text(
                  'There is nothing more...',
                  style: TextStyle(color: Colors.deepOrange),
                ),
              ))
      ],
    );
  }

  void handleEvent(AdmobAdEvent event) {
    switch (event) {
      case AdmobAdEvent.loaded:
        if (bannerAdFailed == true) {
          setState(() {
            bannerAdFailed = false;
          });
        }
        break;
      case AdmobAdEvent.opened:
        print('opened');
        break;
      case AdmobAdEvent.closed:
        print('closed');
        break;
      case AdmobAdEvent.failedToLoad:
        if (bannerAdFailed == false) {
          setState(() {
            bannerAdFailed = true;
          });
        }
        break;
      default:
    }
  }

  @override
  void dispose() {
    super.dispose();
    _scrollController.dispose();
  }
}
