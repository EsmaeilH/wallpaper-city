import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wall_paper/DB/database.dart';
import 'package:wall_paper/services/globals.dart';
import 'package:wall_paper/viewPage.dart';
import 'package:lottie/lottie.dart';
import 'package:wall_paper/model/fetchImage.dart';

class FavouritePage extends StatefulWidget {
  @override
  _FavouritePageState createState() => _FavouritePageState();
}

class _FavouritePageState extends State<FavouritePage> {
  ScrollController _scrollController = new ScrollController();
  var db = DatabaseHelper.instance;
  var allRows;

  @override
  void initState() {
    super.initState();
    allRows = db.queryAllRows();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    allRows = db.queryAllRows();
    return Scaffold(
        body: FutureBuilder<List>(
          future: allRows,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              if (snapshot.data.length > 0){
                return CustomScrollView(
                  controller: _scrollController,
                  slivers: <Widget>[
                    SliverAppBar(
                      floating: true,
                      flexibleSpace: FlexibleSpaceBar(
                        centerTitle: true,
                        title: Text('Favorite', style: TextStyle(fontSize: MediaQuery.of(context).size.width/18),),
                      ),
                    ),
                    SliverGrid(
                      delegate: SliverChildBuilderDelegate(
                            (BuildContext context, int index) => Padding(
                              padding: const EdgeInsets.all(1),
                              child: Stack(
                          children: <Widget>[
                              Container(
                                child: Image.network(
                                  snapshot.data[index]['smallLink'],
                                  height: double.maxFinite,
                                  width: double.maxFinite,
                                  fit: BoxFit.cover,
                                ),
                              ),
                              Material(
                                type: MaterialType.transparency,
                                child: InkWell(
                                  onTap: () async {
                                    previousPage = 'favoritePage';
                                    var thumbsInstance = ImageResponseThumbs(
                                      large: snapshot.data[index]['smallLink'],
                                      original: snapshot.data[index]['smallLink'],
                                      small: snapshot.data[index]['smallLink'],
                                    );
                                    var imageResponseInstance = ImageResponse(
                                      id: snapshot.data[index]['id'],
                                      thumbs: thumbsInstance,
                                      path: snapshot.data[index]['pathLink'],
                                      resolution: snapshot.data[index]['resolution'],
                                      fileSize: snapshot.data[index]['fileSize'],
                                      views: snapshot.data[index]['views'],
                                      favorites: snapshot.data[index]['favorites'],
                                    );
                                    var result = await Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              ViewPage(imageResponseInstance)),
                                    );
                                    if (result != allRows) {
                                      setState(() {
                                        allRows = result;
                                      });
                                    }
                                  },
                                ),
                              ),
                          ],
                        ),
                            ),
                        childCount: snapshot.data.length,
                      ),
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 3,
                      ),
                    ),
                  ],
                );
              }
              else{
                return Center(
                  child: Lottie.asset('assets/empty.json')
                );
              }
            }
            return Center(child: CircularProgressIndicator());
          },
        )
    );
  }
}
