import 'dart:async';
import 'dart:ui';
import 'dart:math';

import 'package:admob_flutter/admob_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'favouritePage.dart';
import 'package:wall_paper/DB/database.dart';
import 'package:wall_paper/services/drawer.dart';
import 'package:wall_paper/setting.dart';
import 'package:connectivity/connectivity.dart';
import 'package:wall_paper/viewPage.dart';
import 'package:wall_paper/favouritePage.dart';
import 'package:wall_paper/services/globals.dart';
import 'package:wall_paper/model/fetchImage.dart';
import 'package:lottie/lottie.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:wall_paper/services/admobService.dart';
import 'package:debug_mode/debug_mode.dart';
import 'package:cached_network_image/cached_network_image.dart';

class WallPaperMainPage extends StatefulWidget {
  static var instance = DatabaseHelper.instance;

  WallPaperMainPage({Key key}) : super(key: key);

  @override
  WallPaperMainPageState createState() => WallPaperMainPageState();
}

class WallPaperMainPageState extends State<WallPaperMainPage> {
  final List arrRoute = ['Airplane', 'Sport', 'Dark', 'Animal', 'Space'];
  bool isInternetOn = true;
  String _connectionStatus = 'Unknown';
  var index = Random().nextInt(5);
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;
  String category;

  Future<ResponsePhoto> resp;

  ScrollController _scrollController;

  int pageNumber = 1;
  int totalHits = 0;
  List imagelist = [];
  bool bannerAdFailed = false;
  final admobService = AdmobService();

  void initState() {
    // TODO: implement initState
    super.initState();

    category = arrRoute[index];
    initConnectivity();
    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);

    if (imagelist.length == 0) {
      resp = fetchPost(category, pageNumber, resolution);
    }

    _scrollController = ScrollController();
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
              _scrollController.position.maxScrollExtent &&
          imagelist.length < totalHits) {
        setState(() {
          pageNumber++;
          resp = fetchPost(category, pageNumber, resolution);
        });
      }
    });
  }

  Future<void> initConnectivity() async {
    ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.mobile:
        setState(() {
          _connectionStatus = '';
        });
        break;
      case ConnectivityResult.wifi:
        setState(() {
          _connectionStatus = '';
        });
        break;
      case ConnectivityResult.none:
        setState(() => _connectionStatus = result.toString());
        break;
      default:
        setState(() => _connectionStatus = 'Failed to get connectivity.');
        break;
    }
  }

  @override
  Widget build(BuildContext context) {

    // preventing from changing portrait mode
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: _appBar(),
      drawer: WallpDrawer(),
      body: FutureBuilder<ResponsePhoto>(
        future: resp,
        builder: (context, obj) {
          if (obj.hasData) {
            if (!imagelist.contains(obj.data.images[0])) {
              totalHits = obj.data.meta.total;
              imagelist.addAll(obj.data.images);
            }

            return _staggeredGridWidget();
          } else if (obj.hasError) {
            return Center(child: Lottie.asset('assets/empty.json'));
          }
          return Center(child: CircularProgressIndicator());
        },
      ),
    );
  }

  Widget _appBar() {
    return AppBar(
      title: (_connectionStatus == '')
          ? null
          : Text('Waiting for connection...'),
      actions: <Widget>[
        GestureDetector(
          child: IconButton(
              icon: new Icon(Icons.favorite_border),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => FavouritePage(),
                    ));
              }),
        ),
        GestureDetector(
            child: IconButton(
                icon: Icon(
                  Icons.settings,
                ),
                color: Colors.deepOrange,
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => Setting(),
                      ));
                }))
      ],
      centerTitle: true,
      backgroundColor: Colors.transparent,
      elevation: 1,
    );
  }

  Widget _staggeredGridWidget() {
    return CustomScrollView(
      controller: _scrollController,
      slivers: [
        SliverStaggeredGrid.countBuilder(
          crossAxisCount: 3,
          staggeredTileBuilder: (int index) => new StaggeredTile.count(3, 2),
          itemBuilder: (BuildContext context, int index) {
            return (index % 5 == 0 && index != 0 && !bannerAdFailed && !isRewarded)
                ? AdmobBanner(
                    adUnitId: DebugMode.isInDebugMode
                        ? admobService.getBannerAdUnitIdTest()
                        : admobService.getBannerAdUnitId(),
                    adSize: AdmobBannerSize.MEDIUM_RECTANGLE,
                    listener: (AdmobAdEvent event, Map<String, dynamic> args) {
                      handleEvent(event);
                    },
                  )
                : Stack(
                    children: <Widget>[
                      Container(
                        child: CachedNetworkImage(
                          imageUrl: imagelist[index].thumbs.large,
                          imageBuilder: (context, imageProvider) => Container(
                            decoration: BoxDecoration(
                                image: DecorationImage(
                              image: imageProvider,
                              fit: BoxFit.cover,
                            )),
                          ),
                        ),
                      ),
                      Material(
                        type: MaterialType.transparency,
                        child: InkWell(
                          onTap: () {
                            previousPage = 'secondPage';
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      ViewPage(imagelist[index])),
                            );
                          },
                        ),
                      )
                    ],
                  );
          },
          itemCount: imagelist.length,
          mainAxisSpacing: 4,
          crossAxisSpacing: 4,
        ),
        imagelist.length < totalHits
            ? SliverToBoxAdapter(
                child: Center(child: CircularProgressIndicator()),
              )
            : SliverToBoxAdapter(
                child: Center(
                child: Text(
                  'There is nothing more...',
                  style: TextStyle(color: Colors.deepOrange),
                ),
              ))
      ],
    );
  }

  void handleEvent(AdmobAdEvent event) {
    switch (event) {
      case AdmobAdEvent.loaded:
        if (bannerAdFailed == true) {
          setState(() {
            bannerAdFailed = false;
          });
        }
        break;
      case AdmobAdEvent.opened:
        print('opened');
        break;
      case AdmobAdEvent.closed:
        print('closed');
        break;
      case AdmobAdEvent.failedToLoad:
        if (bannerAdFailed == false) {
          setState(() {
            bannerAdFailed = true;
          });
        }
        break;
      default:
    }
  }

  void dispose() {
    _connectivitySubscription.cancel();
    _scrollController.dispose();
    super.dispose();
  }
}
